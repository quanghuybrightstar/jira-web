const AuthLayoutWrapper = ({children}) => {
    return (
        <div className={'d-flex'}>
            {children}
        </div>
    )
}

export default AuthLayoutWrapper;