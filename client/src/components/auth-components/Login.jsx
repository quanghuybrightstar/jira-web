import { AuthLayoutWrapper } from "~/layouts/auth-layouts";
import { Link, useNavigate } from "react-router-dom";
import Logo from "~/assets/images/logo.svg";
import { Button, Divider, Form, Input, message } from "antd";
import { useState } from "react";
import { emptyValidation } from "./validation/auth-validations";

const Login = () => {
  const [isLoading, setLoading] = useState(false);

  return (
    <AuthLayoutWrapper>
      <div className={"login"}>
        <div className={"form-wrapper"}>
          <div className="form-header">
            <div className="logo-app-warpper">
              <img src={Logo} alt="logo Jira" className={"logo-app"} />
            </div>
            <div className="app-name">Jira Web</div>
          </div>

          <div className="form-login">
            <Divider className={"divider"} plain>
              Đăng nhập
            </Divider>

            <Form
              name="basic"
              layout="vertical"
              initialValues={{ remember: true }}
              autoComplete="off"
            >
              <Form.Item
                label={<p className={"form-label"}>Tên tài khoản</p>}
                name="username"
                rules={[emptyValidation]}
              >
                <Input name="username" className={"form-input"}></Input>
              </Form.Item>

              <Form.Item
                label={<p className={"form-label"}>Mật khẩu</p>}
                name="password"
                rules={[emptyValidation]}              >
                <Input.Password
                  name="password"
                  className={"form-input"}
                ></Input.Password>
              </Form.Item>

              <div className={"manage-password-login"}>
                <div className={"remember-password"}>
                  <input
                    id={"remember-password"}
                    className={"input-remember-password"}
                    type={"checkbox"}
                  />
                  <label
                    style={{ cursor: "pointer" }}
                    htmlFor="remember-password"
                  >
                    &nbsp;Ghi nhớ
                  </label>
                </div>
                <Link>Quên mật khẩu</Link>
              </div>

              <Form.Item>
                <Button className={"form-submit-btn"} type="primary" htmlType="submit">
                  Đăng nhập
                </Button>
              </Form.Item>

              <div className="manage-register">
                <p className="mr-2">Bạn chưa có tài khoản?</p>
                <Link className="redirect-register" to={"/register"}>
                  Tạo tài khoản mới
                </Link>
              </div>
            </Form>
          </div>
        </div>
        <div className="background-login"></div>
      </div>
    </AuthLayoutWrapper>
  );
};

export default Login;
