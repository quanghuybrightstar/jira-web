import { strongPasswordRegex, emailRegex } from "./auth-regex";

export const emptyValidation = {
    validator: async (_, value) => {
        if(!value) return  Promise.reject(
            new Error ('Không được bỏ trống thông tin!')
        );
        if(value.trim() !== "") {
            return Promise.resolve();
        }
        return Promise.reject(
            new Error ('Thông tin không được để là khoảng trắng!')
        )
    }
};

export const strongPasswordValidation = {
    validator: async (_, value) => {
        if (value) {
            const password = value.trim();
            const checkIsValid = strongPasswordRegex.test(password);
            if(!checkIsValid && password) return Promise.reject(
                new Error('Mật khẩu không đúng định dạng!')
            )
            return Promise.resolve();
        }
    }
}

export const emailValidation =  {
    validator: async (_, value) => {
        if(value) {
            const email = value.trim();
            const checkIsValid = emailRegex.test(email);
            if(!checkIsValid && email) return Promise.reject(
                new Error('Email không đúng định dạng!')
            )
            return Promise.resolve();
        }
    }
}