import { AuthLayoutWrapper } from "~/layouts/auth-layouts";
import Logo from "~/assets/images/logo.svg";
import { Button, Divider, Form, Input, message } from "antd";
import { Link } from "react-router-dom";
import { emptyValidation, emailValidation, strongPasswordValidation } from "./validation/auth-validations";

const Register = () => {
  return (
    <AuthLayoutWrapper>
      <div className="register">
        <div className="form-wrapper">
          <div className="form-register">
            <div className="form-header">
              <div className="logo-app-wrapper">
                <img src={Logo} alt="logo Jira" className={"logo-app"} />
              </div>
              <div className="app-name">Jira Web</div>
            </div>
            <Divider className={"divider"}>Đăng ký tài khoản</Divider>

            <Form
              name="basic"
              layout="vertical"
              initialValues={{ remember: true }}
              autoComplete="off"
            >
              <Form.Item
                label={<p className={"form-label"}>Họ tên</p>}
                name="fullName"
                rules={[emptyValidation]}              >
                <Input className={"form-input"} name="fullName"></Input>
              </Form.Item>

              <Form.Item
                label={<p className={"form-label"}>Email</p>}
                name="email"
                rules={[emptyValidation, emailValidation]}
              >
                <Input className={"form-input"} name="email"></Input>
              </Form.Item>

              <div className="d-flex">
                <Form.Item
                  label={<p className={"form-label"}>Mật khẩu</p>}
                  name="password"
                  style={{ marginRight: 12 }}
                  rules={[emptyValidation, strongPasswordValidation]}
                >
                  <Input.Password name="password" className={"form-input"} />
                </Form.Item>
                <Form.Item
                  label={<p className={"form-label"}>Xác nhận mật khẩu</p>}
                  name="confirmPassword"
                  dependencies={["password"]}
                  rules={[emptyValidation]}
                >
                  <Input.Password
                    name="confirmPassword"
                    className={"form-input"}
                  />
                </Form.Item>
              </div>

              <div className={"remind-form-password"}>
                Mật khẩu ít nhất 8 ký tự bao gồm chữ cái, số, kí tự đặc biệt.
              </div>

              <Form.Item>
                <Button
                  className={"form-submit-btn"}
                  type="primary"
                  htmlType="submit"
                >
                  Đăng ký
                </Button>
              </Form.Item>
              <div className="manage-register">
                <p className="mr-2">Bạn đã có tài khoản?</p>
                <Link className="redirect-register" to={"/login"}>
                  Đăng nhập
                </Link>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </AuthLayoutWrapper>
  );
};

export default Register;
