import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { storeHistoryPath, removeHistoryPath } from "../store/Slices/loginSlice";


const useLogin = () => {
    const $dispatch = useDispatch();
    const $navigate = useNavigate();
    const historyPath = useSelector((state) => state.login.historyPath);

    return {
        subscribe: (path) => {
            if(path !== '/login' && path) $dispatch(storeHistoryPath);
            $navigate('login');
        },
        return: () => {
            $navigate(historyPath);
        },
        logOut: () => {
            $dispatch(removeHistoryPath());
            $navigate("/login");
        }
    }
}

export {useLogin};