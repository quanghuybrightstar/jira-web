import { createSlice } from "@reduxjs/toolkit";

const loginSlice = createSlice({
    name: 'login',
    initialState: {
        historyPath: '/',
    },
    reducers: {
        storeHistoryPath: (state, {payload}) => {
            state.historyPath = payload;
        },
        removeHistoryPath: (state) => {
            state.historyPath = "/";
        },
    }
})

export const { storeHistoryPath, removeHistoryPath} = loginSlice.actions;

export default loginSlice.reducer;