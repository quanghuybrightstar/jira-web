import { Routes, Route, Outlet } from "react-router-dom";
import {useEffect} from 'react'
import LoginPage from "./pages/auth-pages/LoginPage";
import RegisterPage from "./pages/auth-pages/RegisterPage";
import { useLogin } from "./hooks/useLogin";

function App() {
  const $login = useLogin();

  useEffect(() => {
    if(window.location.pathname !== '/login') {
      $login.subscribe({...{...window}.location}.pathname);
    }
  }, [])

  return (
    <Routes>
      <Route exact path="login" element = {<LoginPage />}/>
      <Route exact path="register" element = {<RegisterPage />}/>
    </Routes>
  );
}

export default App;
